class TaskManager:
    def __init__(self):
        self.stack = {}

    def new_task(self,task,priority):
        if priority in self.stack:
            self.stack[priority] += "; " + task
        else:
            self.stack[priority] = task

    def remove(self,priority):
        removed = self.stack.pop(priority)
        return removed

    def str(self):
        list_priority = list(self.stack.keys())
        list_priority.sort()
        result = str()
        for keys in list_priority:
            result += str(f"{keys} {self.stack[keys]}\n")
        return result


manager = TaskManager()
manager.new_task("сделать уборку", 4)
manager.new_task("помыть посуду", 4)
manager.new_task("отдохнуть", 1)
manager.new_task("поесть", 2)
manager.new_task("сдать дз", 2)
manager.remove(2)
print(manager)